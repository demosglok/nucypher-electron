# nucypher electron

This is a simple Electron application based on nucypher mocknet and nucypherjs wrapper

## To Use
you need python3.6 or later
```bash
# Clone this repository
git clone https://gitlab.com/demosglok/nucypher-electron
# Go into the repository
cd nucypher-electron
# Install dependencies
npm install
# setup python virtualenv
./python-init.sh
# Run the app
npm start
```
