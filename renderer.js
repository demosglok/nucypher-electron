// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const {ipcRenderer} = require('electron')

console.log('renderer')
const errorEl = document.getElementById('error')

const genkeyBtn = document.getElementById('genkey')
const gensignkeyBtn = document.getElementById('gensignkey')
const encryptBtn = document.getElementById('encrypt')
const grantBtn = document.getElementById('grant')
const reencryptBtn = document.getElementById('reencrypt')
const decryptBtn = document.getElementById('decrypt')
const revokeBtn = document.getElementById('revoke')

const privkeyInput = document.getElementById('privkey')
const pubkeyInput = document.getElementById('pubkey')
const signPrivkeyInput = document.getElementById('sign_privkey')
const signPubkeyInput = document.getElementById('sign_pubkey')
const dataText = document.getElementById('data')
const targetPubkeyInput = document.getElementById('target_pubkey')
const sourcePubkeyInput = document.getElementById('source_pubkey')
const sourceSignkeyInput = document.getElementById('source_signkey')

const capsuleText = document.getElementById('capsule')
const policyidInput = document.getElementById('policy_id')

const cfragsEl = document.getElementById('cfrags')

const commondata = {}

genkeyBtn.addEventListener('click', () => {
    console.log('genkey')
    ipcRenderer.send('nucypher-request', {type: 'genkey'})
})
gensignkeyBtn.addEventListener('click', () => {
    console.log('gensignkey')
    ipcRenderer.send('nucypher-request', { type: 'gensignkey'})
})
encryptBtn.addEventListener('click', () => {
    console.log('encrypt')
    const pubkey = pubkeyInput.value
    const data = dataText.value
    ipcRenderer.send('nucypher-request', {type: 'encrypt', pubkey, data})
})
grantBtn.addEventListener('click', () => {
    console.log('grant')
    const granter_privkey = privkeyInput.value
    const granter_sign_privkey = signPrivkeyInput.value
    const granted_pubkey = targetPubkeyInput.value
    ipcRenderer.send('nucypher-request', {type: 'grant', granter_privkey, granter_sign_privkey, granted_pubkey})
})
reencryptBtn.addEventListener('click', () => {
    console.log('reencrypt')
    const granter_pubkey = sourcePubkeyInput.value || pubkeyInput.value
    const granter_sign_pubkey = sourceSignkeyInput.value || signPubkeyInput.value
    const granted_pubkey = targetPubkeyInput.value || pubkeyInput.value
    const capsule = capsuleText.value
    const policy_id = policyidInput.value
    ipcRenderer.send('nucypher-request', {
      type: 'reencrypt',
      granter_pubkey,
      granter_sign_pubkey,
      granted_pubkey,
      capsule,
      policy_id
    })
})
decryptBtn.addEventListener('click', () => {
    console.log('decrypt')
    const granter_pubkey = sourcePubkeyInput.value
    const granter_sign_pubkey = sourceSignkeyInput.value
    const granted_privkey = privkeyInput.value
    const capsule = capsuleText.value
    const cfrags = commondata.cfrags
    const encrypted = dataText.value

    ipcRenderer.send('nucypher-request', {
      type: 'decrypt',
      granter_pubkey: '',
      granter_sign_pubkey: '',
      granted_privkey: '',
      capsule: '',
      cfrags: '',
      encrypted: ''
    })
})
revokeBtn.addEventListener('click', () => {
    console.log('revoke')
    const policy_id = policyidInput.value
    ipcRenderer.send('nucypher-request', {type: 'revoke', policy_id})
})

function genkeyHandler(arg){
  pubkeyInput.value = arg.keys.pubkey
  privkeyInput.value = arg.keys.privkey
}
function gensignkeyHandler(arg){
  signPubkeyInput.value = arg.keys.pubkey
  signPrivkeyInput.value = arg.keys.privkey
}
function encryptHandler(arg){
  dataText.value = arg.encrypted.ciphertext
  capsuleText.value = arg.encrypted.capsule
}
function grantHandler(arg){
  policyidInput.value = arg.policy_id
}
function reencryptHandler(arg){
  commondata.cfrags = arg.cfrags
  cfrags.innerHtml = arg.cfrags.map(cfrag => `<input type="text" readonly value="${cfrag}" />`).join(' ')
}
function decryptHandler(arg){
  dataText = arg.decrypted
}
function revokeHandler(arg){
  alert('revoked successfully')
}

ipcRenderer.on('nucypher-reply', (event, arg) => {
  console.log('got response', arg)
  if(arg.error) {
    errorEl.innerText = arg.error
  } else {
    switch(arg.type) {
      case 'genkey':
        genkeyHandler(arg)
      break;
      case 'gensignkey':
        gensignkeyHandler(arg)
      break;
      case 'encrypt':
        encryptHandler(arg)
      break;
      case 'grant':
        grantHandler(arg)
      break;
      case 'reencrypt':
        reencryptHandler(arg)
      break;
      case 'decrypt':
        decryptHandler(arg)
      break;
      case 'revoke':
        revokeHandler(arg)
      break;
    }
  }
})
