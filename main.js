// Modules to control application life and create native browser window
const {app, ipcMain, BrowserWindow} = require('electron')
const nu = require('./nucypher-nodejs')

const debug = /--debug/.test(process.argv[2])
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({width: 800, height: 600})

  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  if (debug) {
        mainWindow.webContents.openDevTools()
        mainWindow.maximize()
        require('devtron').install()
  }
  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
    nu.destroy();
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
  nu.destroy();
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
const ipcdata = {};
ipcMain.on('nucypher-request', async (event, arg, params) => {
  console.log('request', arg)
  try {
    switch(arg.type){
      case 'genkey':
      case 'gensignkey':
        const keys = await nu.genkey();
        event.sender.send('nucypher-reply', {type: arg.type, keys});
      break;
      case 'encrypt':
        if(arg.pubkey && arg.data) {
          const encrypted = await nu.encrypt(arg.pubkey, arg.data);
          event.sender.send('nucypher-reply', {type: arg.type, encrypted});
        } else {
          event.sender.send('nucypher-reply', {error: 'incomplete data'});
        }
      break;
      case 'grant':
        if(arg.granter_privkey && arg.granter_sign_privkey && arg.granted_pubkey) {
          const policy_id = await nu.grant(arg.granter_privkey, arg.granter_sign_privkey, arg.granted_pubkey);
          event.sender.send('nucypher-reply', {type: arg.type, policy_id});
        } else {
          event.sender.send('nucypher-reply', {error: 'incomplete data'});
        }
      break;
      case 'reencrypt':
        if(arg.granter_pubkey && arg.granter_sign_pubkey && arg.granted_pubkey && arg.capsule && arg.policy_id) {
          const cfrags = await nu.reencrypt(
            arg.granter_pubkey,
            arg.granter_sign_pubkey,
            arg.granted_pubkey,
            arg.capsule,
            arg.policy_id
          );
          ipcdata.cfrags = cfrags;
          event.sender.send('nucypher-reply', {type: arg.type, cfrags});
        } else {
          event.sender.send('nucypher-reply', {error: 'incomplete data'});
        }
      break;
      case 'decrypt':
        if(arg.granter_pubkey && arg.granter_sign_pubkey && arg.granted_privkey && arg.capsule && arg.cfrags && arg.encrypted) {
          const decrypted = await nu.decrypt(
            arg.granter_pubkey,
            arg.granter_sign_pubkey,
            arg.granter_privkey,
            arg.capsule,
            arg.cfrags,
            arg.encrypted
          );
          event.sender.send('nucypher-reply', {type: arg.type, decrypted});
        } else {
          event.sender.send('nucypher-reply', {error: 'incomplete data'});
        }
      break;
      case 'revoke':
        if(arg.policy_id) {
          await nu.revoke(arg.policy_id);
          event.sender.send('nucypher-reply', {type: arg.type, revoked: true});
        }  else {
          event.sender.send('nucypher-reply', {error: 'incomplete data, no policy_id'});
        }
      break;
    }
  } catch(ex) {
    event.sender.send('nucypher-reply', {error: ex.message})
  }
})
