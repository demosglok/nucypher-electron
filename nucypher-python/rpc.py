# 0. Imports
import sys, json
from umbral import pre, keys, config
from umbral.signing import Signer
from umbral.cfrags import CapsuleFrag

from nucypher import MockNetwork


# 1. Setup pyUmbral
config.set_default_curve()
mock_kms = MockNetwork()

def gen_key_pair():
    keypair = {}
    privkey = keys.UmbralPrivateKey.gen_key()
    keypair["privkey"] = privkey.to_bytes().hex()
    keypair["pubkey"] = privkey.get_pubkey().to_bytes().hex()
    return keypair

while True:
    try:
        input_str = input()
        command = json.loads(input_str)
        response = json.dumps({'status': 'failure'})
        if command["command"] == 'exit':
            sys.exit(0)
        elif command["command"] == 'genkey':
            response = json.dumps({'status': 'success', 'result': gen_key_pair()})
        elif command["command"] == 'encrypt':
            pubkey = keys.UmbralPublicKey.from_bytes(bytes.fromhex(command["params"]["pubkey"]))
            data = bytes(command["params"]["data"],'utf-8')
            ciphertext, capsule = pre.encrypt(pubkey, data)
            encrypted = {}
            encrypted["ciphertext"] = ciphertext.hex()
            encrypted["capsule"] = capsule.to_bytes().hex()
            response = json.dumps({'status': 'success', 'result': encrypted})
        elif command["command"] == 'grant':
            bob_pubkey = keys.UmbralPublicKey.from_bytes(bytes.fromhex(command["params"]["bob_pubkey"]))
            alice_privkey = keys.UmbralPrivateKey.from_bytes(bytes.fromhex(command["params"]["alice_privkey"]))
            alice_signing_privkey = keys.UmbralPrivateKey.from_bytes(bytes.fromhex(command["params"]["alice_signer"]))
            alice_signer = Signer(alice_signing_privkey)
            alice_kfrags = pre.generate_kfrags(
                delegating_privkey=alice_privkey,
                signer=alice_signer,
                receiving_pubkey=bob_pubkey,
                threshold=10,
                N=20)
            policy_id = mock_kms.grant(alice_kfrags)

            response = json.dumps({'status': 'success', 'result': policy_id})
        elif command["command"] == "reencrypt": #actually this is probably done automatically in case of real network
            alice_pubkey = keys.UmbralPublicKey.from_bytes(bytes.fromhex(command["params"]["alice_pubkey"]))
            alice_signing_pubkey = keys.UmbralPublicKey.from_bytes(bytes.fromhex(command["params"]["alice_signing_pubkey"]))
            bob_pubkey = keys.UmbralPublicKey.from_bytes(bytes.fromhex(command["params"]["bob_pubkey"]))
            policy_id = command["params"]["policy_id"]
            bob_capsule = pre.Capsule.from_bytes(bytes.fromhex(command["params"]["bob_capsule"]), alice_pubkey.params)

            bob_capsule.set_correctness_keys(alice_pubkey, bob_pubkey, alice_signing_pubkey)
            try:
                bob_cfrags = list(map(lambda frag: frag.to_bytes().hex(), mock_kms.reencrypt(policy_id, bob_capsule, 10)))
                response = json.dumps({'status': 'success', 'result': bob_cfrags})
            except ValueError:
                response = json.dumps({'status': 'failure', 'error': 'failed to decrypt'})

        elif command["command"] == 'decrypt':
            alice_pubkey = keys.UmbralPublicKey.from_bytes(bytes.fromhex(command["params"]["alice_pubkey"]))
            alice_signing_pubkey = keys.UmbralPublicKey.from_bytes(bytes.fromhex(command["params"]["alice_signing_pubkey"]))
            bob_capsule = pre.Capsule.from_bytes(bytes.fromhex(command["params"]["bob_capsule"]), alice_pubkey.params)
            bob_privkey = keys.UmbralPrivateKey.from_bytes(bytes.fromhex(command["params"]["bob_privkey"]))
            ciphertext = bytes.fromhex(command["params"]["ciphertext"])
            bob_cfrags = list(map(lambda frag: CapsuleFrag.from_bytes(bytes.fromhex(frag)), command["params"]["cfrags"]))

            bob_capsule.set_correctness_keys(alice_pubkey, bob_pubkey, alice_signing_pubkey)
            for cfrag in bob_cfrags:
                bob_capsule.attach_cfrag(cfrag)

            decrypted_data = pre.decrypt(ciphertext, bob_capsule, bob_privkey, alice_signing_pubkey).decode('utf-8')
            response = json.dumps({'status': 'success', 'result': decrypted_data})
        elif command["command"] == "revoke":
            policy_id = command["params"]["policy_id"]

            mock_kms.revoke(policy_id)
            response = json.dumps({'status': 'success'})

        sys.stdout.write(response + "\n")
        sys.stdout.flush()

    except ValueError:
        sys.stderr.write(json.dumps({'error': 'failed to parse input'}))
        sys.stderr.flush()
    except EOFError:
        sys.exit(0)
